<?php

namespace Cloud\Claimant;

use Cloud\Db\Model\Claimants;
use Respect\Validation\Validator as v;

class Claimant
{
	/**
	 * @var int|null $id
	 */
	private ?int $id = null;

	/**
	 * @var string $nameFirst
	 */
	private string $nameFirst;

	/**
	 * @var string $nameLast
	 */
	private string $nameLast;

	/**
	 * @var string $email
	 */
	private string $email;

	/**
	 * @var string $policyNumber
	 */
	private string $policyNumber;

	/**
	 * Claimant constructor.
	 * @param string $nameFirst
	 * @param string $nameLast
	 * @param string $email
	 * @param string $policyNumber
	 * @param int|null $id
	 */
	public function __construct(string $nameFirst, string $nameLast, string $email, string $policyNumber, ?int $id = null)
	{
		$this->nameFirst = $nameFirst;
		$this->nameLast = $nameLast;
		$this->email = $email;
		$this->policyNumber = $policyNumber;

		if (!empty($id))
		{
			$this->id = $id;
		}
	}

	/**
	 * @return object
	 */
	public function output(): object
	{
		return (object)[
			'id' => $this->id,
			'nameFirst' => $this->nameFirst,
			'nameLast' => $this->nameLast,
			'email' => $this->email,
			'policyNumber' => $this->policyNumber
		];
	}

	/**
	 * @return Claimant
	 * @throws \Exception
	 */
	public function create(): Claimant
	{
		if (!empty($this->id))
		{
			throw new \Exception('user is already created');
		}

		// is first name a string?
		if (!v::stringType()->validate($this->nameFirst))
		{
			throw new \Exception('first name should be a string');
		}

		// has first name at least 2 chars?
		if (!v::stringType()->length(2, null)->validate($this->nameFirst))
		{
			throw new \Exception('first name min length is 2');
		}

		// is last name a string?
		if (!v::stringType()->validate($this->nameLast))
		{
			throw new \Exception('last name should be a string');
		}

		// has first name at least 2 chars?
		if (!v::stringType()->length(2, null)->validate($this->nameLast))
		{
			throw new \Exception('last name min length is 2');
		}

		// is email correct?
		if (!v::email()->validate($this->email))
		{
			throw new \Exception('email is incorrect');
		}

		// has policy number at least 1 char and no spaces?
		if (!v::noWhitespace()->length(1, null)->validate($this->policyNumber))
		{
			throw new \Exception('policy number has spaces');
		}

		// try to create a db row
		try {
			$claimant = new Claimants();
			$claimant
				->setFirstName($this->nameFirst)
				->setLastName($this->nameLast)
				->setEmail($this->email)
				->setPolicyNumber($this->policyNumber)
				->save();

			$this->id = $claimant->getId();
		} catch (\Exception $e) {
			throw new \Exception('can\'t create claimant: ' . $e->getMessage());
		}

		return $this;
	}
}