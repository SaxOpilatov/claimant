<?php

namespace Cloud\Enum;

use Spatie\Enum\Enum;

/**
 * Class InterruptionReasonEnum
 * @package Cloud\Enum
 * ----------------------------
 * @method static self other()
 * @method static self weather()
 * @method static self technical_mechanical()
 * @method static self staffing()
 * @method static self security()
 */
class InterruptionReasonEnum extends Enum
{

}