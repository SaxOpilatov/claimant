<?php

namespace Cloud\Claim;

use Cloud\Claimant\Claimant;
use Cloud\Enum\InterruptionConsequenceEnum;
use Cloud\Enum\InterruptionReasonEnum;
use Respect\Validation\Validator as v;

class Claim
{
	/**
	 * @var Claimant $claimant
	 */
	protected Claimant $claimant;

	/**
	 * @var string $originalFlightNumber
	 */
	protected string $originalFlightNumber;

	/**
	 * @var \DateTime $originalFlightDate
	 */
	protected \DateTime $originalFlightDate;

	/**
	 * @var InterruptionReasonEnum $flightInterruptionReason
	 */
	protected InterruptionReasonEnum $flightInterruptionReason;

	/**
	 * @var InterruptionConsequenceEnum $flightInterruptionConsequence
	 */
	protected InterruptionConsequenceEnum $flightInterruptionConsequence;

	/**
	 * Claim constructor.
	 * @param Claimant $claimant
	 * @param string $originalFlightNumber
	 * @param string $originalFlightDate
	 * @param int|string $flightInterruptionReason
	 * @param int|string $flightInterruptionConsequence
	 * @throws \Exception
	 */
	public function __construct(
		Claimant $claimant,
		string $originalFlightNumber,
		string $originalFlightDate,
		$flightInterruptionReason,
		$flightInterruptionConsequence
	)
	{
		$this->claimant = $claimant;
		$this->originalFlightNumber = $originalFlightNumber;
		$this->originalFlightDate = new \DateTime($originalFlightDate);
		$this->flightInterruptionReason = InterruptionReasonEnum::make($flightInterruptionReason);
		$this->flightInterruptionConsequence = InterruptionConsequenceEnum::make($flightInterruptionConsequence);
	}

	/**
	 * @param Claimant $claimant
	 * @param string $originalFlightNumber
	 * @param string $originalFlightDate
	 * @param int|string $flightInterruptionReason
	 * @param int|string $flightInterruptionConsequence
	 * @param string|null $delayTime
	 * @param string|null $cancellationFlightNumber
	 * @param string|null $cancellationFlightDate
	 * @return Claim
	 * @throws \Exception
	 */
	public static function build(
		Claimant $claimant,
		string $originalFlightNumber,
		string $originalFlightDate,
		$flightInterruptionReason,
		$flightInterruptionConsequence,
		?string $delayTime,
		?string $cancellationFlightNumber,
		?string $cancellationFlightDate
	): Claim
	{
		$consequence = InterruptionConsequenceEnum::make($flightInterruptionConsequence);
		switch ($consequence)
		{
			case InterruptionConsequenceEnum::cancellation():
				return new ClaimCancellation($claimant, $originalFlightNumber, $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence, $cancellationFlightNumber, $cancellationFlightDate);

			case InterruptionConsequenceEnum::delay():
				return new ClaimDelay($claimant, $originalFlightNumber, $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence, $delayTime);

			default:
				throw new \Exception('unknown cancellation consequence');
		}
	}

	/**
	 * @return Claim
	 * @throws \Exception
	 */
	public function create(): Claim
	{
		// has flight number at least 1 char and no spaces?
		if (!v::noWhitespace()->length(1, null)->validate($this->originalFlightNumber))
		{
			throw new \Exception('policy number has spaces');
		}

		// check date
		$today = new \DateTime();
		$diff = $this->originalFlightDate->diff($today)->format('%a');
		if ($diff > 45)
		{
			throw new \Exception('max 45 days ago');
		}
		if ($today < $this->originalFlightDate)
		{
			throw new \Exception('flight date can\'t be in the future');
		}

		return $this;
	}

	/**
	 * @return object
	 */
	public function output(): object
	{
		return (object)[
			'claimant' => $this->claimant->output(),
			'flight' => [
				'original_number' => $this->originalFlightNumber,
				'original_date' => $this->originalFlightDate,
				'interruption_reason' => $this->flightInterruptionReason,
				'interruption_consequence' => $this->flightInterruptionConsequence
			]
		];
	}
}