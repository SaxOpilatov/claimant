<?php

namespace Cloud\Claim;

use Cloud\Claimant\Claimant;
use Cloud\Db\Model\Claims;

class ClaimDelay extends Claim
{
	/**
	 * @var string $delayTime
	 */
	private string $delayTime;

	/**
	 * ClaimDelay constructor.
	 * @param Claimant $claimant
	 * @param string $originalFlightNumber
	 * @param string $originalFlightDate
	 * @param int|string $flightInterruptionReason
	 * @param int|string $flightInterruptionConsequence
	 * @param string $delayTime
	 * @throws \Exception
	 */
	public function __construct(Claimant $claimant, string $originalFlightNumber, string $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence, string $delayTime)
	{
		parent::__construct($claimant, $originalFlightNumber, $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence);
		$this->delayTime = $delayTime;
	}

	/**
	 * @return object
	 */
	public function output(): object
	{
		$data = parent::output();
		$data->delay_time = $this->delayTime;

		return $data;
	}

	/**
	 * @return Claim
	 * @throws \Exception
	 */
	public function create(): Claim
	{
		parent::create();

		try {
			$claim = new Claims();
			$claim
				->setClaimantId($this->claimant->output()->id)
				->setOriginalFlightNumber($this->originalFlightNumber)
				->setOriginalFlightDate($this->originalFlightDate)
				->setFlightInterruptionReason($this->flightInterruptionReason->getIndex())
				->setFlightInterruptionConsequence($this->flightInterruptionConsequence->getIndex())
				->setFlightDelayTime($this->delayTime)
				->save();
		} catch (\Exception $e) {
			throw new \Exception('can\'t create delay claim: ' . $e->getMessage());
		}

		return $this;
	}
}