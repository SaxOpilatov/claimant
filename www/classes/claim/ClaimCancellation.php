<?php

namespace Cloud\Claim;

use Cloud\Claimant\Claimant;
use Cloud\Db\Model\Claims;

class ClaimCancellation extends Claim
{
	/**
	 * @var string $cancellationFlightNumber
	 */
	private string $cancellationFlightNumber;

	/**
	 * @var \DateTime $cancellationFlightDate
	 */
	private \DateTime $cancellationFlightDate;

	/**
	 * ClaimCancellation constructor.
	 * @param Claimant $claimant
	 * @param string $originalFlightNumber
	 * @param string $originalFlightDate
	 * @param int|string $flightInterruptionReason
	 * @param int|string $flightInterruptionConsequence
	 * @param string $cancellationFlightNumber
	 * @param string $cancellationFlightDate
	 * @throws \Exception
	 */
	public function __construct(Claimant $claimant, string $originalFlightNumber, string $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence, string $cancellationFlightNumber, string $cancellationFlightDate)
	{
		parent::__construct($claimant, $originalFlightNumber, $originalFlightDate, $flightInterruptionReason, $flightInterruptionConsequence);
		$this->cancellationFlightNumber = $cancellationFlightNumber;
		$this->cancellationFlightDate = new \DateTime($cancellationFlightDate);
	}

	/**
	 * @return object
	 * @throws \Exception
	 */
	public function output(): object
	{
		$data = parent::output();

		$data->cancellation_flight_number = $this->cancellationFlightNumber;
		$data->cancellation_flight_date = $this->cancellationFlightDate;

		return $data;
	}

	/**
	 * @return Claim
	 * @throws \Exception
	 */
	public function create(): Claim
	{
		parent::create();

		if ($this->cancellationFlightDate < $this->originalFlightDate)
		{
			throw new \Exception('new flight date can\'t be less than original');
		}

		try {
			$claim = new Claims();
			$claim
				->setClaimantId($this->claimant->output()->id)
				->setOriginalFlightNumber($this->originalFlightNumber)
				->setOriginalFlightDate($this->originalFlightDate)
				->setFlightInterruptionReason($this->flightInterruptionReason->getIndex())
				->setFlightInterruptionConsequence($this->flightInterruptionConsequence->getIndex())
				->setFlightCancellationNewFlightNumber($this->cancellationFlightNumber)
				->setFlightCancellationNewFlightDate($this->cancellationFlightDate)
				->save();
		} catch (\Exception $e) {
			throw new \Exception('can\'t create delay claim: ' . $e->getMessage());
		}

		return $this;
	}
}