<?php

namespace Cloud\Db;

use Propel\Runtime\Connection\ConnectionManagerSingle;
use Propel\Runtime\Propel;

class Orm
{
	public static function getEntity()
	{
		$serviceContainer = Propel::getServiceContainer();
		$serviceContainer->checkVersion('2.0.0-dev');
		$serviceContainer->setAdapterClass('default', 'pgsql');

		$manager = new ConnectionManagerSingle();
		$manager->setConfiguration([
			'dsn' => "pgsql:host=db;port=5432;dbname={$_SERVER['POSTGRES_DB']}",
			'user' => $_SERVER['POSTGRES_USER'],
			'password' => $_SERVER['POSTGRES_PASSWORD'],
			'settings' => [
				'charset' => 'utf8',
				'queries' => []
			],
			'classname' => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
			'model_paths' => [
				0 => 'src',
				1 => 'vendor',
			]
		]);

		$manager->setName('default');
		$serviceContainer->setConnectionManager('default', $manager);
		$serviceContainer->setDefaultDatasource('default');
	}

	public static function getConf(): array
	{
		$dbHost = 'db';
		$dbPort = 5432;
		$dbName = $_SERVER['POSTGRES_DB'];
		$dbUser = $_SERVER['POSTGRES_USER'];
		$dbPass = $_SERVER['POSTGRES_PASSWORD'];

		return [
			'propel' => [
				'paths' => [
					'schemaDir' => __DIR__,
					'phpDir' => __DIR__ . '/models',
				],
				'database' => [
					'connections' => [
						'default' => [
							'adapter' => 'pgsql',
							'dsn' => "pgsql:host={$dbHost};port={$dbPort};dbname={$dbName}",
							'user' => $dbUser,
							'password' => $dbPass,
							'settings' => [
								'charset' => 'utf8'
							]
						]
					]
				]
			]
		];
	}
}