<?php

namespace Cloud\Db\Model;

use Cloud\Db\Model\Base\ClaimsQuery as BaseClaimsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'claims' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ClaimsQuery extends BaseClaimsQuery
{

}
