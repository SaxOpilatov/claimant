<?php

namespace Cloud\Db\Model\Map;

use Cloud\Db\Model\Claims;
use Cloud\Db\Model\ClaimsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'claims' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ClaimsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Cloud.Db.Model.Map.ClaimsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'claims';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Cloud\\Db\\Model\\Claims';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Cloud.Db.Model.Claims';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'claims.id';

    /**
     * the column name for the claimant_id field
     */
    const COL_CLAIMANT_ID = 'claims.claimant_id';

    /**
     * the column name for the original_flight_number field
     */
    const COL_ORIGINAL_FLIGHT_NUMBER = 'claims.original_flight_number';

    /**
     * the column name for the original_flight_date field
     */
    const COL_ORIGINAL_FLIGHT_DATE = 'claims.original_flight_date';

    /**
     * the column name for the flight_interruption_reason field
     */
    const COL_FLIGHT_INTERRUPTION_REASON = 'claims.flight_interruption_reason';

    /**
     * the column name for the flight_interruption_consequence field
     */
    const COL_FLIGHT_INTERRUPTION_CONSEQUENCE = 'claims.flight_interruption_consequence';

    /**
     * the column name for the flight_cancellation_new_flight_number field
     */
    const COL_FLIGHT_CANCELLATION_NEW_FLIGHT_NUMBER = 'claims.flight_cancellation_new_flight_number';

    /**
     * the column name for the flight_cancellation_new_flight_date field
     */
    const COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE = 'claims.flight_cancellation_new_flight_date';

    /**
     * the column name for the flight_delay_time field
     */
    const COL_FLIGHT_DELAY_TIME = 'claims.flight_delay_time';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ClaimantId', 'OriginalFlightNumber', 'OriginalFlightDate', 'FlightInterruptionReason', 'FlightInterruptionConsequence', 'FlightCancellationNewFlightNumber', 'FlightCancellationNewFlightDate', 'FlightDelayTime', ),
        self::TYPE_CAMELNAME     => array('id', 'claimantId', 'originalFlightNumber', 'originalFlightDate', 'flightInterruptionReason', 'flightInterruptionConsequence', 'flightCancellationNewFlightNumber', 'flightCancellationNewFlightDate', 'flightDelayTime', ),
        self::TYPE_COLNAME       => array(ClaimsTableMap::COL_ID, ClaimsTableMap::COL_CLAIMANT_ID, ClaimsTableMap::COL_ORIGINAL_FLIGHT_NUMBER, ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE, ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON, ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE, ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_NUMBER, ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE, ClaimsTableMap::COL_FLIGHT_DELAY_TIME, ),
        self::TYPE_FIELDNAME     => array('id', 'claimant_id', 'original_flight_number', 'original_flight_date', 'flight_interruption_reason', 'flight_interruption_consequence', 'flight_cancellation_new_flight_number', 'flight_cancellation_new_flight_date', 'flight_delay_time', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ClaimantId' => 1, 'OriginalFlightNumber' => 2, 'OriginalFlightDate' => 3, 'FlightInterruptionReason' => 4, 'FlightInterruptionConsequence' => 5, 'FlightCancellationNewFlightNumber' => 6, 'FlightCancellationNewFlightDate' => 7, 'FlightDelayTime' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'claimantId' => 1, 'originalFlightNumber' => 2, 'originalFlightDate' => 3, 'flightInterruptionReason' => 4, 'flightInterruptionConsequence' => 5, 'flightCancellationNewFlightNumber' => 6, 'flightCancellationNewFlightDate' => 7, 'flightDelayTime' => 8, ),
        self::TYPE_COLNAME       => array(ClaimsTableMap::COL_ID => 0, ClaimsTableMap::COL_CLAIMANT_ID => 1, ClaimsTableMap::COL_ORIGINAL_FLIGHT_NUMBER => 2, ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE => 3, ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON => 4, ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE => 5, ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_NUMBER => 6, ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE => 7, ClaimsTableMap::COL_FLIGHT_DELAY_TIME => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'claimant_id' => 1, 'original_flight_number' => 2, 'original_flight_date' => 3, 'flight_interruption_reason' => 4, 'flight_interruption_consequence' => 5, 'flight_cancellation_new_flight_number' => 6, 'flight_cancellation_new_flight_date' => 7, 'flight_delay_time' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('claims');
        $this->setPhpName('Claims');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Cloud\\Db\\Model\\Claims');
        $this->setPackage('Cloud.Db.Model');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('claims_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('claimant_id', 'ClaimantId', 'INTEGER', 'claimants', 'id', true, null, null);
        $this->addColumn('original_flight_number', 'OriginalFlightNumber', 'VARCHAR', true, 128, null);
        $this->addColumn('original_flight_date', 'OriginalFlightDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('flight_interruption_reason', 'FlightInterruptionReason', 'SMALLINT', true, null, null);
        $this->addColumn('flight_interruption_consequence', 'FlightInterruptionConsequence', 'SMALLINT', true, null, null);
        $this->addColumn('flight_cancellation_new_flight_number', 'FlightCancellationNewFlightNumber', 'VARCHAR', false, 128, null);
        $this->addColumn('flight_cancellation_new_flight_date', 'FlightCancellationNewFlightDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('flight_delay_time', 'FlightDelayTime', 'VARCHAR', false, 128, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Claimants', '\\Cloud\\Db\\Model\\Claimants', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':claimant_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ClaimsTableMap::CLASS_DEFAULT : ClaimsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Claims object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ClaimsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ClaimsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ClaimsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ClaimsTableMap::OM_CLASS;
            /** @var Claims $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ClaimsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ClaimsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ClaimsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Claims $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ClaimsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ClaimsTableMap::COL_ID);
            $criteria->addSelectColumn(ClaimsTableMap::COL_CLAIMANT_ID);
            $criteria->addSelectColumn(ClaimsTableMap::COL_ORIGINAL_FLIGHT_NUMBER);
            $criteria->addSelectColumn(ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE);
            $criteria->addSelectColumn(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON);
            $criteria->addSelectColumn(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE);
            $criteria->addSelectColumn(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_NUMBER);
            $criteria->addSelectColumn(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE);
            $criteria->addSelectColumn(ClaimsTableMap::COL_FLIGHT_DELAY_TIME);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.claimant_id');
            $criteria->addSelectColumn($alias . '.original_flight_number');
            $criteria->addSelectColumn($alias . '.original_flight_date');
            $criteria->addSelectColumn($alias . '.flight_interruption_reason');
            $criteria->addSelectColumn($alias . '.flight_interruption_consequence');
            $criteria->addSelectColumn($alias . '.flight_cancellation_new_flight_number');
            $criteria->addSelectColumn($alias . '.flight_cancellation_new_flight_date');
            $criteria->addSelectColumn($alias . '.flight_delay_time');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ClaimsTableMap::DATABASE_NAME)->getTable(ClaimsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ClaimsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ClaimsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ClaimsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Claims or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Claims object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Cloud\Db\Model\Claims) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ClaimsTableMap::DATABASE_NAME);
            $criteria->add(ClaimsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ClaimsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ClaimsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ClaimsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the claims table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ClaimsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Claims or Criteria object.
     *
     * @param mixed               $criteria Criteria or Claims object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Claims object
        }

        if ($criteria->containsKey(ClaimsTableMap::COL_ID) && $criteria->keyContainsValue(ClaimsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ClaimsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ClaimsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ClaimsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ClaimsTableMap::buildTableMap();
