<?php

namespace Cloud\Db\Model\Base;

use \Exception;
use \PDO;
use Cloud\Db\Model\Claims as ChildClaims;
use Cloud\Db\Model\ClaimsQuery as ChildClaimsQuery;
use Cloud\Db\Model\Map\ClaimsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'claims' table.
 *
 *
 *
 * @method     ChildClaimsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildClaimsQuery orderByClaimantId($order = Criteria::ASC) Order by the claimant_id column
 * @method     ChildClaimsQuery orderByOriginalFlightNumber($order = Criteria::ASC) Order by the original_flight_number column
 * @method     ChildClaimsQuery orderByOriginalFlightDate($order = Criteria::ASC) Order by the original_flight_date column
 * @method     ChildClaimsQuery orderByFlightInterruptionReason($order = Criteria::ASC) Order by the flight_interruption_reason column
 * @method     ChildClaimsQuery orderByFlightInterruptionConsequence($order = Criteria::ASC) Order by the flight_interruption_consequence column
 * @method     ChildClaimsQuery orderByFlightCancellationNewFlightNumber($order = Criteria::ASC) Order by the flight_cancellation_new_flight_number column
 * @method     ChildClaimsQuery orderByFlightCancellationNewFlightDate($order = Criteria::ASC) Order by the flight_cancellation_new_flight_date column
 * @method     ChildClaimsQuery orderByFlightDelayTime($order = Criteria::ASC) Order by the flight_delay_time column
 *
 * @method     ChildClaimsQuery groupById() Group by the id column
 * @method     ChildClaimsQuery groupByClaimantId() Group by the claimant_id column
 * @method     ChildClaimsQuery groupByOriginalFlightNumber() Group by the original_flight_number column
 * @method     ChildClaimsQuery groupByOriginalFlightDate() Group by the original_flight_date column
 * @method     ChildClaimsQuery groupByFlightInterruptionReason() Group by the flight_interruption_reason column
 * @method     ChildClaimsQuery groupByFlightInterruptionConsequence() Group by the flight_interruption_consequence column
 * @method     ChildClaimsQuery groupByFlightCancellationNewFlightNumber() Group by the flight_cancellation_new_flight_number column
 * @method     ChildClaimsQuery groupByFlightCancellationNewFlightDate() Group by the flight_cancellation_new_flight_date column
 * @method     ChildClaimsQuery groupByFlightDelayTime() Group by the flight_delay_time column
 *
 * @method     ChildClaimsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClaimsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClaimsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClaimsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClaimsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClaimsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClaimsQuery leftJoinClaimants($relationAlias = null) Adds a LEFT JOIN clause to the query using the Claimants relation
 * @method     ChildClaimsQuery rightJoinClaimants($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Claimants relation
 * @method     ChildClaimsQuery innerJoinClaimants($relationAlias = null) Adds a INNER JOIN clause to the query using the Claimants relation
 *
 * @method     ChildClaimsQuery joinWithClaimants($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Claimants relation
 *
 * @method     ChildClaimsQuery leftJoinWithClaimants() Adds a LEFT JOIN clause and with to the query using the Claimants relation
 * @method     ChildClaimsQuery rightJoinWithClaimants() Adds a RIGHT JOIN clause and with to the query using the Claimants relation
 * @method     ChildClaimsQuery innerJoinWithClaimants() Adds a INNER JOIN clause and with to the query using the Claimants relation
 *
 * @method     \Cloud\Db\Model\ClaimantsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildClaims findOne(ConnectionInterface $con = null) Return the first ChildClaims matching the query
 * @method     ChildClaims findOneOrCreate(ConnectionInterface $con = null) Return the first ChildClaims matching the query, or a new ChildClaims object populated from the query conditions when no match is found
 *
 * @method     ChildClaims findOneById(int $id) Return the first ChildClaims filtered by the id column
 * @method     ChildClaims findOneByClaimantId(int $claimant_id) Return the first ChildClaims filtered by the claimant_id column
 * @method     ChildClaims findOneByOriginalFlightNumber(string $original_flight_number) Return the first ChildClaims filtered by the original_flight_number column
 * @method     ChildClaims findOneByOriginalFlightDate(string $original_flight_date) Return the first ChildClaims filtered by the original_flight_date column
 * @method     ChildClaims findOneByFlightInterruptionReason(int $flight_interruption_reason) Return the first ChildClaims filtered by the flight_interruption_reason column
 * @method     ChildClaims findOneByFlightInterruptionConsequence(int $flight_interruption_consequence) Return the first ChildClaims filtered by the flight_interruption_consequence column
 * @method     ChildClaims findOneByFlightCancellationNewFlightNumber(string $flight_cancellation_new_flight_number) Return the first ChildClaims filtered by the flight_cancellation_new_flight_number column
 * @method     ChildClaims findOneByFlightCancellationNewFlightDate(string $flight_cancellation_new_flight_date) Return the first ChildClaims filtered by the flight_cancellation_new_flight_date column
 * @method     ChildClaims findOneByFlightDelayTime(string $flight_delay_time) Return the first ChildClaims filtered by the flight_delay_time column *

 * @method     ChildClaims requirePk($key, ConnectionInterface $con = null) Return the ChildClaims by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOne(ConnectionInterface $con = null) Return the first ChildClaims matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClaims requireOneById(int $id) Return the first ChildClaims filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByClaimantId(int $claimant_id) Return the first ChildClaims filtered by the claimant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByOriginalFlightNumber(string $original_flight_number) Return the first ChildClaims filtered by the original_flight_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByOriginalFlightDate(string $original_flight_date) Return the first ChildClaims filtered by the original_flight_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByFlightInterruptionReason(int $flight_interruption_reason) Return the first ChildClaims filtered by the flight_interruption_reason column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByFlightInterruptionConsequence(int $flight_interruption_consequence) Return the first ChildClaims filtered by the flight_interruption_consequence column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByFlightCancellationNewFlightNumber(string $flight_cancellation_new_flight_number) Return the first ChildClaims filtered by the flight_cancellation_new_flight_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByFlightCancellationNewFlightDate(string $flight_cancellation_new_flight_date) Return the first ChildClaims filtered by the flight_cancellation_new_flight_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaims requireOneByFlightDelayTime(string $flight_delay_time) Return the first ChildClaims filtered by the flight_delay_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClaims[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildClaims objects based on current ModelCriteria
 * @method     ChildClaims[]|ObjectCollection findById(int $id) Return ChildClaims objects filtered by the id column
 * @method     ChildClaims[]|ObjectCollection findByClaimantId(int $claimant_id) Return ChildClaims objects filtered by the claimant_id column
 * @method     ChildClaims[]|ObjectCollection findByOriginalFlightNumber(string $original_flight_number) Return ChildClaims objects filtered by the original_flight_number column
 * @method     ChildClaims[]|ObjectCollection findByOriginalFlightDate(string $original_flight_date) Return ChildClaims objects filtered by the original_flight_date column
 * @method     ChildClaims[]|ObjectCollection findByFlightInterruptionReason(int $flight_interruption_reason) Return ChildClaims objects filtered by the flight_interruption_reason column
 * @method     ChildClaims[]|ObjectCollection findByFlightInterruptionConsequence(int $flight_interruption_consequence) Return ChildClaims objects filtered by the flight_interruption_consequence column
 * @method     ChildClaims[]|ObjectCollection findByFlightCancellationNewFlightNumber(string $flight_cancellation_new_flight_number) Return ChildClaims objects filtered by the flight_cancellation_new_flight_number column
 * @method     ChildClaims[]|ObjectCollection findByFlightCancellationNewFlightDate(string $flight_cancellation_new_flight_date) Return ChildClaims objects filtered by the flight_cancellation_new_flight_date column
 * @method     ChildClaims[]|ObjectCollection findByFlightDelayTime(string $flight_delay_time) Return ChildClaims objects filtered by the flight_delay_time column
 * @method     ChildClaims[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClaimsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Cloud\Db\Model\Base\ClaimsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Cloud\\Db\\Model\\Claims', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClaimsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClaimsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClaimsQuery) {
            return $criteria;
        }
        $query = new ChildClaimsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildClaims|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClaimsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClaimsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClaims A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, claimant_id, original_flight_number, original_flight_date, flight_interruption_reason, flight_interruption_consequence, flight_cancellation_new_flight_number, flight_cancellation_new_flight_date, flight_delay_time FROM claims WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildClaims $obj */
            $obj = new ChildClaims();
            $obj->hydrate($row);
            ClaimsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildClaims|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClaimsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClaimsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the claimant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClaimantId(1234); // WHERE claimant_id = 1234
     * $query->filterByClaimantId(array(12, 34)); // WHERE claimant_id IN (12, 34)
     * $query->filterByClaimantId(array('min' => 12)); // WHERE claimant_id > 12
     * </code>
     *
     * @see       filterByClaimants()
     *
     * @param     mixed $claimantId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByClaimantId($claimantId = null, $comparison = null)
    {
        if (is_array($claimantId)) {
            $useMinMax = false;
            if (isset($claimantId['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_CLAIMANT_ID, $claimantId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($claimantId['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_CLAIMANT_ID, $claimantId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_CLAIMANT_ID, $claimantId, $comparison);
    }

    /**
     * Filter the query on the original_flight_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalFlightNumber('fooValue');   // WHERE original_flight_number = 'fooValue'
     * $query->filterByOriginalFlightNumber('%fooValue%', Criteria::LIKE); // WHERE original_flight_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalFlightNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByOriginalFlightNumber($originalFlightNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalFlightNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_ORIGINAL_FLIGHT_NUMBER, $originalFlightNumber, $comparison);
    }

    /**
     * Filter the query on the original_flight_date column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalFlightDate('2011-03-14'); // WHERE original_flight_date = '2011-03-14'
     * $query->filterByOriginalFlightDate('now'); // WHERE original_flight_date = '2011-03-14'
     * $query->filterByOriginalFlightDate(array('max' => 'yesterday')); // WHERE original_flight_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $originalFlightDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByOriginalFlightDate($originalFlightDate = null, $comparison = null)
    {
        if (is_array($originalFlightDate)) {
            $useMinMax = false;
            if (isset($originalFlightDate['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE, $originalFlightDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originalFlightDate['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE, $originalFlightDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_ORIGINAL_FLIGHT_DATE, $originalFlightDate, $comparison);
    }

    /**
     * Filter the query on the flight_interruption_reason column
     *
     * Example usage:
     * <code>
     * $query->filterByFlightInterruptionReason(1234); // WHERE flight_interruption_reason = 1234
     * $query->filterByFlightInterruptionReason(array(12, 34)); // WHERE flight_interruption_reason IN (12, 34)
     * $query->filterByFlightInterruptionReason(array('min' => 12)); // WHERE flight_interruption_reason > 12
     * </code>
     *
     * @param     mixed $flightInterruptionReason The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByFlightInterruptionReason($flightInterruptionReason = null, $comparison = null)
    {
        if (is_array($flightInterruptionReason)) {
            $useMinMax = false;
            if (isset($flightInterruptionReason['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON, $flightInterruptionReason['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($flightInterruptionReason['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON, $flightInterruptionReason['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_REASON, $flightInterruptionReason, $comparison);
    }

    /**
     * Filter the query on the flight_interruption_consequence column
     *
     * Example usage:
     * <code>
     * $query->filterByFlightInterruptionConsequence(1234); // WHERE flight_interruption_consequence = 1234
     * $query->filterByFlightInterruptionConsequence(array(12, 34)); // WHERE flight_interruption_consequence IN (12, 34)
     * $query->filterByFlightInterruptionConsequence(array('min' => 12)); // WHERE flight_interruption_consequence > 12
     * </code>
     *
     * @param     mixed $flightInterruptionConsequence The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByFlightInterruptionConsequence($flightInterruptionConsequence = null, $comparison = null)
    {
        if (is_array($flightInterruptionConsequence)) {
            $useMinMax = false;
            if (isset($flightInterruptionConsequence['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE, $flightInterruptionConsequence['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($flightInterruptionConsequence['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE, $flightInterruptionConsequence['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_INTERRUPTION_CONSEQUENCE, $flightInterruptionConsequence, $comparison);
    }

    /**
     * Filter the query on the flight_cancellation_new_flight_number column
     *
     * Example usage:
     * <code>
     * $query->filterByFlightCancellationNewFlightNumber('fooValue');   // WHERE flight_cancellation_new_flight_number = 'fooValue'
     * $query->filterByFlightCancellationNewFlightNumber('%fooValue%', Criteria::LIKE); // WHERE flight_cancellation_new_flight_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flightCancellationNewFlightNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByFlightCancellationNewFlightNumber($flightCancellationNewFlightNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flightCancellationNewFlightNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_NUMBER, $flightCancellationNewFlightNumber, $comparison);
    }

    /**
     * Filter the query on the flight_cancellation_new_flight_date column
     *
     * Example usage:
     * <code>
     * $query->filterByFlightCancellationNewFlightDate('2011-03-14'); // WHERE flight_cancellation_new_flight_date = '2011-03-14'
     * $query->filterByFlightCancellationNewFlightDate('now'); // WHERE flight_cancellation_new_flight_date = '2011-03-14'
     * $query->filterByFlightCancellationNewFlightDate(array('max' => 'yesterday')); // WHERE flight_cancellation_new_flight_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $flightCancellationNewFlightDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByFlightCancellationNewFlightDate($flightCancellationNewFlightDate = null, $comparison = null)
    {
        if (is_array($flightCancellationNewFlightDate)) {
            $useMinMax = false;
            if (isset($flightCancellationNewFlightDate['min'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE, $flightCancellationNewFlightDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($flightCancellationNewFlightDate['max'])) {
                $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE, $flightCancellationNewFlightDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_CANCELLATION_NEW_FLIGHT_DATE, $flightCancellationNewFlightDate, $comparison);
    }

    /**
     * Filter the query on the flight_delay_time column
     *
     * Example usage:
     * <code>
     * $query->filterByFlightDelayTime('fooValue');   // WHERE flight_delay_time = 'fooValue'
     * $query->filterByFlightDelayTime('%fooValue%', Criteria::LIKE); // WHERE flight_delay_time LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flightDelayTime The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByFlightDelayTime($flightDelayTime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flightDelayTime)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimsTableMap::COL_FLIGHT_DELAY_TIME, $flightDelayTime, $comparison);
    }

    /**
     * Filter the query by a related \Cloud\Db\Model\Claimants object
     *
     * @param \Cloud\Db\Model\Claimants|ObjectCollection $claimants The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClaimsQuery The current query, for fluid interface
     */
    public function filterByClaimants($claimants, $comparison = null)
    {
        if ($claimants instanceof \Cloud\Db\Model\Claimants) {
            return $this
                ->addUsingAlias(ClaimsTableMap::COL_CLAIMANT_ID, $claimants->getId(), $comparison);
        } elseif ($claimants instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClaimsTableMap::COL_CLAIMANT_ID, $claimants->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByClaimants() only accepts arguments of type \Cloud\Db\Model\Claimants or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Claimants relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function joinClaimants($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Claimants');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Claimants');
        }

        return $this;
    }

    /**
     * Use the Claimants relation Claimants object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Cloud\Db\Model\ClaimantsQuery A secondary query class using the current class as primary query
     */
    public function useClaimantsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClaimants($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Claimants', '\Cloud\Db\Model\ClaimantsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildClaims $claims Object to remove from the list of results
     *
     * @return $this|ChildClaimsQuery The current query, for fluid interface
     */
    public function prune($claims = null)
    {
        if ($claims) {
            $this->addUsingAlias(ClaimsTableMap::COL_ID, $claims->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the claims table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClaimsTableMap::clearInstancePool();
            ClaimsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClaimsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClaimsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClaimsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ClaimsQuery
