<?php

namespace Cloud\Db\Model\Base;

use \Exception;
use \PDO;
use Cloud\Db\Model\Claimants as ChildClaimants;
use Cloud\Db\Model\ClaimantsQuery as ChildClaimantsQuery;
use Cloud\Db\Model\Map\ClaimantsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'claimants' table.
 *
 *
 *
 * @method     ChildClaimantsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildClaimantsQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildClaimantsQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildClaimantsQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildClaimantsQuery orderByPolicyNumber($order = Criteria::ASC) Order by the policy_number column
 *
 * @method     ChildClaimantsQuery groupById() Group by the id column
 * @method     ChildClaimantsQuery groupByFirstName() Group by the first_name column
 * @method     ChildClaimantsQuery groupByLastName() Group by the last_name column
 * @method     ChildClaimantsQuery groupByEmail() Group by the email column
 * @method     ChildClaimantsQuery groupByPolicyNumber() Group by the policy_number column
 *
 * @method     ChildClaimantsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClaimantsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClaimantsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClaimantsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClaimantsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClaimantsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClaimantsQuery leftJoinClaims($relationAlias = null) Adds a LEFT JOIN clause to the query using the Claims relation
 * @method     ChildClaimantsQuery rightJoinClaims($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Claims relation
 * @method     ChildClaimantsQuery innerJoinClaims($relationAlias = null) Adds a INNER JOIN clause to the query using the Claims relation
 *
 * @method     ChildClaimantsQuery joinWithClaims($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Claims relation
 *
 * @method     ChildClaimantsQuery leftJoinWithClaims() Adds a LEFT JOIN clause and with to the query using the Claims relation
 * @method     ChildClaimantsQuery rightJoinWithClaims() Adds a RIGHT JOIN clause and with to the query using the Claims relation
 * @method     ChildClaimantsQuery innerJoinWithClaims() Adds a INNER JOIN clause and with to the query using the Claims relation
 *
 * @method     \Cloud\Db\Model\ClaimsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildClaimants findOne(ConnectionInterface $con = null) Return the first ChildClaimants matching the query
 * @method     ChildClaimants findOneOrCreate(ConnectionInterface $con = null) Return the first ChildClaimants matching the query, or a new ChildClaimants object populated from the query conditions when no match is found
 *
 * @method     ChildClaimants findOneById(int $id) Return the first ChildClaimants filtered by the id column
 * @method     ChildClaimants findOneByFirstName(string $first_name) Return the first ChildClaimants filtered by the first_name column
 * @method     ChildClaimants findOneByLastName(string $last_name) Return the first ChildClaimants filtered by the last_name column
 * @method     ChildClaimants findOneByEmail(string $email) Return the first ChildClaimants filtered by the email column
 * @method     ChildClaimants findOneByPolicyNumber(string $policy_number) Return the first ChildClaimants filtered by the policy_number column *

 * @method     ChildClaimants requirePk($key, ConnectionInterface $con = null) Return the ChildClaimants by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaimants requireOne(ConnectionInterface $con = null) Return the first ChildClaimants matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClaimants requireOneById(int $id) Return the first ChildClaimants filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaimants requireOneByFirstName(string $first_name) Return the first ChildClaimants filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaimants requireOneByLastName(string $last_name) Return the first ChildClaimants filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaimants requireOneByEmail(string $email) Return the first ChildClaimants filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClaimants requireOneByPolicyNumber(string $policy_number) Return the first ChildClaimants filtered by the policy_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClaimants[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildClaimants objects based on current ModelCriteria
 * @method     ChildClaimants[]|ObjectCollection findById(int $id) Return ChildClaimants objects filtered by the id column
 * @method     ChildClaimants[]|ObjectCollection findByFirstName(string $first_name) Return ChildClaimants objects filtered by the first_name column
 * @method     ChildClaimants[]|ObjectCollection findByLastName(string $last_name) Return ChildClaimants objects filtered by the last_name column
 * @method     ChildClaimants[]|ObjectCollection findByEmail(string $email) Return ChildClaimants objects filtered by the email column
 * @method     ChildClaimants[]|ObjectCollection findByPolicyNumber(string $policy_number) Return ChildClaimants objects filtered by the policy_number column
 * @method     ChildClaimants[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClaimantsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Cloud\Db\Model\Base\ClaimantsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Cloud\\Db\\Model\\Claimants', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClaimantsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClaimantsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClaimantsQuery) {
            return $criteria;
        }
        $query = new ChildClaimantsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildClaimants|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClaimantsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClaimantsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClaimants A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, first_name, last_name, email, policy_number FROM claimants WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildClaimants $obj */
            $obj = new ChildClaimants();
            $obj->hydrate($row);
            ClaimantsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildClaimants|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClaimantsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClaimantsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ClaimantsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ClaimantsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimantsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimantsTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimantsTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimantsTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the policy_number column
     *
     * Example usage:
     * <code>
     * $query->filterByPolicyNumber('fooValue');   // WHERE policy_number = 'fooValue'
     * $query->filterByPolicyNumber('%fooValue%', Criteria::LIKE); // WHERE policy_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $policyNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByPolicyNumber($policyNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($policyNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClaimantsTableMap::COL_POLICY_NUMBER, $policyNumber, $comparison);
    }

    /**
     * Filter the query by a related \Cloud\Db\Model\Claims object
     *
     * @param \Cloud\Db\Model\Claims|ObjectCollection $claims the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClaimantsQuery The current query, for fluid interface
     */
    public function filterByClaims($claims, $comparison = null)
    {
        if ($claims instanceof \Cloud\Db\Model\Claims) {
            return $this
                ->addUsingAlias(ClaimantsTableMap::COL_ID, $claims->getClaimantId(), $comparison);
        } elseif ($claims instanceof ObjectCollection) {
            return $this
                ->useClaimsQuery()
                ->filterByPrimaryKeys($claims->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClaims() only accepts arguments of type \Cloud\Db\Model\Claims or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Claims relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function joinClaims($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Claims');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Claims');
        }

        return $this;
    }

    /**
     * Use the Claims relation Claims object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Cloud\Db\Model\ClaimsQuery A secondary query class using the current class as primary query
     */
    public function useClaimsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClaims($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Claims', '\Cloud\Db\Model\ClaimsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildClaimants $claimants Object to remove from the list of results
     *
     * @return $this|ChildClaimantsQuery The current query, for fluid interface
     */
    public function prune($claimants = null)
    {
        if ($claimants) {
            $this->addUsingAlias(ClaimantsTableMap::COL_ID, $claimants->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the claimants table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimantsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClaimantsTableMap::clearInstancePool();
            ClaimantsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClaimantsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClaimantsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClaimantsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClaimantsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ClaimantsQuery
