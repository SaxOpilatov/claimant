<?php

use Cloud\Db\Orm;

require_once __DIR__ . '/vendor/autoload.php';

return Orm::getConf();