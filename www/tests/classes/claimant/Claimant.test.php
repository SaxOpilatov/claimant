<?php

namespace Cloud\Tests\Claimant;

use Cloud\Claimant\Claimant;
use Cloud\Db\Orm;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class ClaimantTest extends TestCase
{
	public function test_can_create_claimant()
	{
		// db
		Orm::getEntity();

		// fake data
		$faker = Factory::create();

		$claimant = new Claimant(
			$faker->firstName,
			$faker->lastName,
			$faker->email,
			$faker->swiftBicNumber
		);
		$data = $claimant->create();

		$this->assertIsInt($data->output()->id);
	}
}