<?php

namespace Cloud\Tests\Claimant;

use Cloud\Claim\Claim;
use Cloud\Claimant\Claimant;
use Cloud\Db\Orm;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class ClaimTest extends TestCase
{
	public function test_claim_build_delay_int()
	{
		// db
		Orm::getEntity();

		// fake data
		$faker = Factory::create();

		// claimant
		$claimant = new Claimant(
			$faker->firstName,
			$faker->lastName,
			$faker->email,
			$faker->swiftBicNumber
		);
		$claimant->create();

		// date
		$date = new \DateTime();

		$claim = Claim::build(
			$claimant,
			$faker->uuid,
			$date->format('Y-m-d'),
			2,
			1,
			$faker->dayOfWeek(),
			null,
			null
		);
		$claim->create();

		$this->assertObjectHasAttribute('delay_time', $claim->output());
	}

	public function test_claim_build_delay_str()
	{
		// db
		Orm::getEntity();

		// fake data
		$faker = Factory::create();

		// claimant
		$claimant = new Claimant(
			$faker->firstName,
			$faker->lastName,
			$faker->email,
			$faker->swiftBicNumber
		);
		$claimant->create();

		// date
		$date = new \DateTime();

		$claim = Claim::build(
			$claimant,
			$faker->uuid,
			$date->format('Y-m-d'),
			2,
			'delay',
			$faker->dayOfWeek(),
			null,
			null
		);
		$claim->create();

		$this->assertObjectHasAttribute('delay_time', $claim->output());
	}

	public function test_claim_build_cancellation_int()
	{
		// db
		Orm::getEntity();

		// fake data
		$faker = Factory::create();

		// claimant
		$claimant = new Claimant(
			$faker->firstName,
			$faker->lastName,
			$faker->email,
			$faker->swiftBicNumber
		);
		$claimant->create();

		// date
		$date = new \DateTime();
		$date2 = clone $date;
		$date2->modify('+2days');

		$claim = Claim::build(
			$claimant,
			$faker->uuid,
			$date->format('Y-m-d'),
			2,
			0,
			null,
			$faker->uuid,
			$date2->format('Y-m-d')
		);
		$claim->create();

		$this->assertObjectHasAttribute('cancellation_flight_date', $claim->output());
		$this->assertObjectHasAttribute('cancellation_flight_number', $claim->output());
	}

	public function test_claim_build_cancellation_str()
	{
		// db
		Orm::getEntity();

		// fake data
		$faker = Factory::create();

		// claimant
		$claimant = new Claimant(
			$faker->firstName,
			$faker->lastName,
			$faker->email,
			$faker->swiftBicNumber
		);
		$claimant->create();

		// date
		$date = new \DateTime();
		$date2 = clone $date;
		$date2->modify('+2days');

		$claim = Claim::build(
			$claimant,
			$faker->uuid,
			$date->format('Y-m-d'),
			2,
			0,
			null,
			$faker->uuid,
			$date2->format('Y-m-d')
		);
		$claim->create();

		$this->assertObjectHasAttribute('cancellation_flight_date', $claim->output());
		$this->assertObjectHasAttribute('cancellation_flight_number', $claim->output());
	}
}