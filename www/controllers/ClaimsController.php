<?php

namespace Cloud\Controller;

use Cloud\Claim\Claim;
use Cloud\Claim\ClaimCancellation;
use Cloud\Claim\ClaimDelay;
use Cloud\Claimant\Claimant;
use Cloud\Db\Model\ClaimsQuery;
use Cloud\Enum\InterruptionConsequenceEnum;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClaimsController
{
	/**
	 * @param Request $request
	 * @param Response $response
	 * @return Response
	 */
	public static function index(Request $request, Response $response): Response
	{
		switch ($request->getMethod())
		{
			case 'GET':
				$data = self::get($response);
				break;

			case 'POST':
				$data = self::create($request, $response);
				break;

			default:
				$data = self::get($response);
		}

		$response->getBody()->write(json_encode($data));
		return $response
			->withHeader('Content-Type', 'application/json');
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return array
	 */
	private static function create(Request $request, Response $response): array
	{
		$data = [];
		$error = "";

		$body = $request->getParsedBody();
		$input = (object)[
			'claimant_name_first' => (!empty($body['claimant_name_first'])) ? $body['claimant_name_first'] : '',
			'claimant_name_last' => (!empty($body['claimant_name_last'])) ? $body['claimant_name_last'] : '',
			'claimant_email' => (!empty($body['claimant_email'])) ? $body['claimant_email'] : '',
			'claimant_policy_number' => (!empty($body['claimant_policy_number'])) ? $body['claimant_policy_number'] : '',
			'original_flight_number' => (!empty($body['original_flight_number'])) ? $body['original_flight_number'] : '',
			'original_flight_date' => (!empty($body['original_flight_date'])) ? $body['original_flight_date'] : '',
			'flight_interruption_reason' => (isset($body['flight_interruption_reason'])) ? $body['flight_interruption_reason'] : '',
			'flight_interruption_consequence' => (isset($body['flight_interruption_consequence'])) ? $body['flight_interruption_consequence'] : '',
			'delay_time' => (!empty($body['delay_time'])) ? $body['delay_time'] : '',
			'cancellation_flight_number' => (!empty($body['cancellation_flight_number'])) ? $body['cancellation_flight_number'] : '',
			'cancellation_flight_date' => (!empty($body['cancellation_flight_date'])) ? $body['cancellation_flight_date'] : ''
		];

		try {
			$claimantObj = new Claimant(
				$input->claimant_name_first,
				$input->claimant_name_last,
				$input->claimant_email,
				$input->claimant_policy_number
			);
			$claimant = $claimantObj->create();

			$claimObj = Claim::build(
				$claimant,
				$input->original_flight_number,
				$input->original_flight_date,
				$input->flight_interruption_reason,
				$input->flight_interruption_consequence,
				$input->delay_time,
				$input->cancellation_flight_number,
				$input->cancellation_flight_date
			);
			$claimObj->create();

			$data['success'] = true;
		} catch (\Exception $e) {
			$error = $e->getMessage();
			$data['success'] = false;
		}

		return [
			'error' => $error,
			'data' => $data
		];
	}

	private static function get(Response $response): array
	{
		$data = [];
		$error = "";

		try {
			$claimsModel = new ClaimsQuery();
			$claimList = $claimsModel::create()
				->orderById('DESC')
				->find();

			if (!empty($claimList))
			{
				foreach ($claimList as $claim)
				{
					$type = InterruptionConsequenceEnum::make($claim->getFlightInterruptionConsequence());
					$claimant = new Claimant(
						$claim->getClaimants()->getFirstName(),
						$claim->getClaimants()->getLastName(),
						$claim->getClaimants()->getEmail(),
						$claim->getClaimants()->getPolicyNumber(),
						$claim->getClaimants()->getId()
					);

					switch ($type)
					{
						case InterruptionConsequenceEnum::cancellation():
							$claimObj = new ClaimCancellation(
								$claimant,
								$claim->getOriginalFlightNumber(),
								$claim->getOriginalFlightDate('Y-m-d'),
								$claim->getFlightInterruptionReason(),
								$claim->getFlightInterruptionConsequence(),
								$claim->getFlightCancellationNewFlightNumber(),
								$claim->getFlightCancellationNewFlightDate('Y-m-d')
							);
							break;

						case InterruptionConsequenceEnum::delay():
							$claimObj = new ClaimDelay(
								$claimant,
								$claim->getOriginalFlightNumber(),
								$claim->getOriginalFlightDate('Y-m-d'),
								$claim->getFlightInterruptionReason(),
								$claim->getFlightInterruptionConsequence(),
								$claim->getFlightDelayTime()
							);
							break;

						default:
							throw new \Exception('incorrect type');
					}

					$data['claims'][] = $claimObj->output();
				}
			}
		} catch (\Exception $e) {
			$error = $e->getMessage();
		}

		return [
			'error' => $error,
			'data' => $data
		];
	}
}