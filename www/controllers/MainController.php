<?php

namespace Cloud\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MainController
{
	/**
	 * @param Request $request
	 * @param Response $response
	 * @return Response
	 */
	public static function index(Request $request, Response $response): Response
	{
		$md = <<<MD
			# GET /

			+ Response 200 (text/html; charset=UTF-8)
			
			    + Body
			
			            HTML
			
			
			# POST /claims
			
			+ Request (application/json; charset=utf-8)
			
			        {
			            "claimant_name_first": <string>,
			            "claimant_name_last": <string>,
			            "claimant_email": <string(email)>",
			            "claimant_policy_number": <string>,
			            "original_flight_number": <string>,
			            "original_flight_date": <string(date)>,
			            "flight_interruption_reason": <string|int>,
			            "flight_interruption_consequence": <string|int>,
			            "delay_time": <string>,
			            "cancellation_flight_number": <string>,
			            "cancellation_flight_date": <string(date)>
			        }
			
			+ Response 200 (application/json)
			
			    + Body
			
			            {
			                "error": <string>,
			                "data": {
			                    "success": <bool>
			                }
			            }
			
			
			# GET /claims
			
			+ Response 200 (application/json)
			
			    + Body
			
			            {
			                "error": <string>,
			                "data": {
			                    "claims":[
			                        {
			                            "claimant": {
			                                "id": <int>,
			                                "nameFirst": <string>,
			                                "nameLast": <string>,
			                                "email": <string>,
			                                "policyNumber": <string>
			                            },
			                            "flight": {
			                                "original_number": <string>,
			                                "original_date": {
			                                    "date": "2019-12-28 00:00:00.000000",
			                                    "timezone_type": 3,
			                                    "timezone": "Europe\/Tallinn"
			                                },
			                                "interruption_reason": <string>,
			                                "interruption_consequence": <string>
			                            },
			                            "cancellation_flight_number": <string>,
			                            "cancellation_flight_date": {
			                                "date":"2019-12-29 00:00:00.000000",
			                                "timezone_type":3,
			                                "timezone":"Europe\/Tallinn"
			                            }
			                        }
			                    ]
			                }
			            }
		MD;

		$response->getBody()->write(\Parsedown::instance()->text($md));
		return $response;
	}
}