<?php

if (!empty($_SERVER['APP_MODE']) && $_SERVER['APP_MODE'] === 'dev')
{
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Cloud\Middleware\JsonBodyParserMiddleware;
use Cloud\Controller\MainController;
use Cloud\Controller\ClaimsController;
use Cloud\Db\Orm;

require __DIR__ . '/../vendor/autoload.php';

// load database
Orm::getEntity();

// run slim framework
$app = AppFactory::create();
$app->add(new JsonBodyParserMiddleware());

$app->get('/', fn(Request $request, Response $response) => MainController::index($request, $response));
$app->map(['GET', 'POST'], '/claims', fn(Request $request, Response $response) => ClaimsController::index($request, $response));

$app->run();