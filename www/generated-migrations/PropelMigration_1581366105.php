<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1581366105.
 * Generated on 2020-02-10 22:21:45 by root
 */
class PropelMigration_1581366105
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
BEGIN;

CREATE TABLE "claims"
(
    "id" serial NOT NULL,
    "claimant_id" INTEGER NOT NULL,
    "original_flight_number" VARCHAR(128) NOT NULL,
    "original_flight_date" TIMESTAMP NOT NULL,
    "flight_interruption_reason" INT2 NOT NULL,
    "flight_interruption_consequence" INT2 NOT NULL,
    "flight_cancellation_new_flight_number" VARCHAR(128),
    "flight_cancellation_new_flight_date" TIMESTAMP,
    "flight_delay_time" VARCHAR(128),
    PRIMARY KEY ("id")
);

ALTER TABLE "claims" ADD CONSTRAINT "claims_fk_ce1c01"
    FOREIGN KEY ("claimant_id")
    REFERENCES "claimants" ("id");

COMMIT;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
BEGIN;

DROP TABLE IF EXISTS "claims" CASCADE;

COMMIT;
',
);
    }

}