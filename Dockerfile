FROM composer:latest AS composer
FROM php:7.4.2-fpm

RUN echo $PHP_INI_DIR

# locales
RUN apt-get update \
	&& apt-get install -y locales

RUN dpkg-reconfigure locales \
	&& locale-gen C.UTF-8 \
	&& /usr/sbin/update-locale LANG=C.UTF-8

RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
	&& locale-gen

# configs for date and opcache
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY ./php/conf.d/date.ini $PHP_INI_DIR/conf.d/

# install libs
RUN apt-get update && apt-get install -y \
    curl \
    git \
    zip \
    unzip \
    libpq-dev \
    libssl-dev \
    libpcre3-dev \
    openssl \
    && docker-php-ext-install pdo pdo_pgsql \
    && docker-php-ext-install exif \
    && docker-php-ext-install opcache

# composer
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN cd /var/www

RUN apt-get clean && rm -rf /var/lib/apt/lists/*
