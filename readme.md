# Run
Just execute `docker-compose up --build -d`

# Setting
## Docker
### 1. Detect docker container with php
`docker ps -a`

### 2. Run bash for this container
`docker exec -it <CONTAINER_ID> bash`

### 3. Go to the work dir
`cd /var/www`

## Composer
### Install libs
`composer update`

### Dump autoload
`composer dump-autoload`

## Migrations
### Run migrations
`./propel migrate`

# Api methods
## http://localhost:8080
Rendered markdown with blueprint api is available with GET request `/`

# Tests
## PHPUnit
### Run tests
`./vendor/bin/phpunit`